package src;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class BD {
	static ArrayList<String> Alumnos;
	public static void processFile(){
		Alumnos = new ArrayList<>();
		
        	FileInputStream inputStream = null;
        	Scanner sc = null;
        	try {
        	
        		System.out.println("\t\t\t\t\t\t   Base de Datos");
        	inputStream = new FileInputStream("/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos.txt");
            sc = new Scanner(inputStream, "UTF-8");
  
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                System.out.println(line);
                Alumnos.add(line);
            }
    		System.out.println("-----------------------------------------------------------------");
        } catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getBD(){
		return Alumnos;
	}

}
