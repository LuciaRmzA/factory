package src;
import src.Main;
import src.Proceso;

public class Main {
	private Proceso procesoBD;
	public static void main(String args[]) throws Exception{
		Main main = new Main();
		main.run();
	}
	
	public void run() throws Exception{
		procesoBD = new Proceso();
		procesoBD.run();
	}
}
