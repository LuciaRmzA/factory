package src;

import Factory.CSV;
import Factory.Formato;
import Factory.JSON;
import Factory.TXT;
import Factory.XML;

public class FactoryConcreta extends FormatoFactory {

	@Override
	public Formato crear(String type) {
		 if("TXT".equals(type)) {
			 return new TXT();
			 }else 
				 if("XML".equals(type)){
					 return new XML();
				 } else 
					 if("CSV".equals(type)){
						 return new CSV();
					 } else {
						 return new JSON();
					 }
		 }
}


