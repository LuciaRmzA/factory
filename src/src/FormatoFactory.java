package src;
import Factory.Formato;

public abstract class FormatoFactory {
	
	public  abstract Formato crear(String type);
	
	public void formato(String type){
		Formato formato = crear(type);
		formato.ExportarFormato();
	}
}
