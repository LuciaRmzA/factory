package src;

import Factory.Formato;
import src.BD;

public class Proceso {
	BD bd;
	
	public Proceso(){
		bd = new BD();
	}
    
	public void run (){
		System.out.println("\t\t       Sandra Lucia Ramirez Avila - FactoryMethod ");
		System.out.println("-----------------------------------------------------------------");
		BD.processFile();
		AplicarFactory();
	}

	private void AplicarFactory() {
		FactoryConcreta FactoryConcreta = new FactoryConcreta();
		
		Formato a1 = FactoryConcreta.crear("TXT");
		System.out.println("formatoTXT: " + a1.ExportarFormato());

		Formato a3 = FactoryConcreta.crear("CSV");
		System.out.println("formatoCSV: " + a3.ExportarFormato());
		
		Formato a2 = FactoryConcreta.crear("XML");
		System.out.println("formatoXML: " + a2.ExportarFormato());
		
		Formato a4 = FactoryConcreta.crear("JSON");
		System.out.println("formatoJSON: " + a4.ExportarFormato());
		

	}

}
