package Factory;

import java.io.File;
import java.io.FileWriter;

import src.BD;

public class CSV extends Formato {

	@Override
	public String ExportarFormato() {
		BD proceso= new BD();
		try {
			File archivo=new File("/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos.csv");
			FileWriter escribir=new FileWriter(archivo,true);
			
			for(int i = 0; i < proceso.getBD().size();i++ ){
				String line = proceso.getBD().get(i);
				String csv = line.replace("-", ",");
	            escribir.append(csv);
	            escribir.append("\n");
	            escribir.flush();
			}
			
			escribir.close();
		} catch(Exception e){
			System.out.println("Error al escribir");
		}
		return "/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos.csv";
	}

}
