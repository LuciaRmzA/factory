package Factory;

import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import src.BD;

public class JSON extends Formato {

	@Override
	public String ExportarFormato() {
		BD proceso= new BD();
		try {
			FileWriter file = new FileWriter("/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos.json");
			
			for(int i = 0; i < proceso.getBD().size();i++ ){
				String line = proceso.getBD().get(i);
				String matricula = null;
		        String nombre = null;
		        String apellidoPaterno = null;
		        String apellidoMaterno = null;
		        String edad = null;
		        String sexo = null;
		        String carrera = null;
		        String semestre = null;
		        
		    	String[] parts = line.split("-");
		    	
		    	matricula = parts[0];
				nombre = parts[1];
				apellidoPaterno = parts[2];
				apellidoMaterno = parts[3];
				edad = parts[4];
				sexo = parts[5];
				carrera = parts[6];
				semestre = parts[7];
    	
				String lineaJson = 
						" [{Matricula : " + matricula + ", Nombre: " + nombre + 
						", Apellido Paterno: " + apellidoPaterno + ", Apellido Mateno:" + apellidoMaterno +
						", Edad: " + edad + ", Sexo: "+ sexo + ", Carrera: " + carrera +
						", Semestre: "+ semestre + "}]";
    	
				
				JSONObject obj = new JSONObject();		
				obj.put("Alumnos["+i+"]", lineaJson);
		
				file.write(obj.toString());
				file.flush();
			}
			file.close();
		} catch (IOException e) {
			System.out.println("Error: " + e);
		} catch (JSONException e) {
			System.out.println("Error JSON: " + e);
		}
		return "/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos.json";
	}

}
