package Factory;

import java.io.File;
import java.io.FileWriter;
import src.BD;

public class TXT extends Formato {

	@Override
	public String ExportarFormato() {
		BD proceso= new BD();
		try {
			File archivo=new File("/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos1.txt");
			FileWriter escribir=new FileWriter(archivo,true);
	    	 
			for(int i = 0; i < proceso.getBD().size();i++ ){
			String line = proceso.getBD().get(i);
			String matricula = null;
	        String nombre = null;
	        String apellidoPaterno = null;
	        String apellidoMaterno = null;
	        String edad = null;
	        String sexo = null;
	        String carrera = null;
	        String semestre = null;
	        
	    	String[] parts = line.split("-");

	        matricula = parts[0];
    		nombre = parts[1];
    		apellidoPaterno = parts[2];
    		apellidoMaterno = parts[3];
    		edad = parts[4];
    		sexo = parts[5];
    		carrera = parts[6];
    		semestre = parts[7];

			escribir.write(matricula);
			escribir.write("\t");
			escribir.write(nombre);
			escribir.write("\t");
			escribir.write(apellidoPaterno);
			escribir.write("\t");
			escribir.write(apellidoMaterno);
			escribir.write("\t");
			escribir.write(edad);
			escribir.write("\t");
			escribir.write(sexo);
			escribir.write("\t");
			escribir.write(carrera);
			escribir.write("\t");
			escribir.write(semestre);
			escribir.write("\n");
			}
			
			escribir.close();

		} catch(Exception e){
			System.out.println("Error al escribir");
		}
		return "/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos1.txt";
	}

}