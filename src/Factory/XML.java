package Factory;

import java.io.FileWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import src.BD;

public class XML extends Formato {
	XMLOutputFactory factory;
	XMLStreamWriter writer;
	@Override
	public String ExportarFormato() {
		BD proceso= new BD();
		
		try {
			factory = XMLOutputFactory .newInstance();
			writer = factory.createXMLStreamWriter(new FileWriter("/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos.xml")); 
			writer.writeStartElement("Alumno");

			for(int i = 0; i < proceso.getBD().size();i++ ){
				String line = proceso.getBD().get(i);
				
				String matricula = null;
	            String nombre = null;
	            String apellidoPaterno = null;
	            String apellidoMaterno = null;
	            String edad = null;
	            String sexo = null;
	            String carrera = null;
	            String semestre = null;
            
	            String[] parts = line.split("-");
        	
	            matricula = parts[0];
	    		nombre = parts[1];
	    		apellidoPaterno = parts[2];
	    		apellidoMaterno = parts[3];
	    		edad = parts[4];
	    		sexo = parts[5];
	    		carrera = parts[6];
	    		semestre = parts[7];
        	
	            writer.writeStartElement("Matricula");
	            writer.writeCharacters(matricula);
	            writer.writeEndElement();
	            writer.writeStartElement("Nombre");
	            writer.writeCharacters(nombre);
	            writer.writeEndElement();
	            writer.writeStartElement("ApellidoPaterno");
	            writer.writeCharacters(apellidoPaterno);
	            writer.writeEndElement();
	            writer.writeStartElement("ApellidoMaterno");
	            writer.writeCharacters(apellidoMaterno);
	            writer.writeEndElement();
	            writer.writeStartElement("Edad");
	            writer.writeCharacters(edad);
	            writer.writeEndElement();
	            writer.writeStartElement("Sexo");
	            writer.writeCharacters(sexo);
	            writer.writeEndElement();
	            writer.writeStartElement("Carrera");
	            writer.writeCharacters(carrera);
	            writer.writeEndElement();
	            writer.writeStartElement("Semestre");
	            writer.writeCharacters(semestre);
	            writer.writeEndElement();

			}
			
            writer.writeEndDocument();
            writer.flush();
            writer.close();
            
		} catch(Exception e){
			System.out.println("Error: " + e);
		}
		return "/Users/MacSalvador/Documents/Eclipse/workspace/FactoryProject/lib/BaseDatos.xml";
	}

}